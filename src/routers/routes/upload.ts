import {Router} from 'express';

import * as UploadController from '../../controller/upload.controller';

const router = Router();

router.post('/profile-picture',UploadController.uploadProfilePicture);

export default router;