import * as SocketController from '../../controller/socket.controller';

export const socketIoRouter = (io:any) => {
    return io.on('connection',(socket:any)=>{

        socket.on("online", (data:any) => 
            SocketController.onlineController(io,socket,data)
        );

        socket.on('message',(data:any) => 
            SocketController.sendMessage(io,socket,data)
        );

        socket.on('send-file',(data:any) => 
            SocketController.sendFile(io,socket,data)
        );

        socket.on('disconnect',() => 
            SocketController.disconnect(io,socket)
        );
        
    })
}