import {Router} from 'express';

import * as SaveMessageController from '../../controller/message.controller';

const router = Router();

router.post('/save-message',SaveMessageController.saveMessage);
router.post('/query-messages',SaveMessageController.queryMessages);
router.post('/send-file',SaveMessageController.sendFile);

export default router;