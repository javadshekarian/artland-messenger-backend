import {Router} from 'express';

import * as ContactController from '../../controller/contact.controller';

const router = Router();

router.post('/create-contact',ContactController.createContact);
router.post('/query-contact',ContactController.queryContact);
router.post('/update-notification',ContactController.updateNotification);
router.post('/query-current-contact',ContactController.queryCurrentContact);

export default router;