import {Router} from 'express';

import * as LoginController from '../../controller/login.controller';

const router = Router();

router.post('/register',LoginController.register);
router.post('/login',LoginController.login);
router.post('/check-token',LoginController.checkToken);
router.post('/query-account-data',LoginController.queryAccountDataWithToken);
router.post('/query-with-id',LoginController.queryAccountDataWithUserID);

export default router;