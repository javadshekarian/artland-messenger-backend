const {Router} = require('express');

import loginRouter from './routes/logins';
import contactRouter from './routes/contact';
import uploadRouter from './routes/upload';
import saveMessageRoute from './routes/message';

const router = new Router();

router.use('/login',loginRouter);
router.use('/upload',uploadRouter);
router.use('/contact',contactRouter);
router.use('/message',saveMessageRoute);

export default router;