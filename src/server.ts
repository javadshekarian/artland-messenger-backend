import path from "path";

import express,{Express} from 'express';
import bodyParser from "body-parser";
import cors from 'cors';
import upload from 'express-fileupload';

import * as ENV from './config/config';
import apiRoute from './routers/index';
import {connectDB} from './database/index';
import {socketIoRouter} from './routers/routes/socket.router'

connectDB();

const app:Express = express();

app.use(express.static(path.join(__dirname,'statics','statics')));
app.use(express.static(path.join(__dirname,'statics','uploads')));

app.use(cors({
    origin:'*',
    methods:["GET","POST","OPTIONS","PUT","PATCH","DELETE"],
    allowedHeaders:['Content-Type', 'Authorization'],
    credentials:true,
    optionsSuccessStatus:200
}));
app.use(bodyParser.urlencoded({extended:false}));
app.use(upload());

app.use('/',apiRoute);

const server = require('http').Server(app);
const io = require('socket.io')(server);

socketIoRouter(io);

server.listen(ENV.PORT, () => {
    console.log(`listening on *:${ENV.PORT}`);
});

const exitHandler = ():void => {
    if (server) {
        server.close(() => {
            console.info("Server closed");
            process.exit(1);
        });
    } else {
        process.exit(1);
    }
};

const unexpectedErrorHandler = (error: unknown):void => {
    console.error(`unexpectedErrorHandler: ${error}`);
    exitHandler();
};

process.on("uncaughtException", unexpectedErrorHandler);
process.on("unhandledRejection", unexpectedErrorHandler);