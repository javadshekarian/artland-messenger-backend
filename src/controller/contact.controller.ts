import { Request,Response } from "express";
import { jwtDecode } from "jwt-decode";

import { userFindOne } from "../service/user.service";
import { contactCreateOne,contactFindOne,contactFindMany,contactFindOneAndUpdate, contactFindOneSelectColumn } from "../service/contact.service";
import { ContactInterface } from "../interface/contact.interface";
import { UserInterface } from "../interface/user.interface";
import { TokenInterface } from "../interface/token.interface";

export const createContact = async (req:Request,res:Response) 
    : Promise<void> => {
    try{
        const {contactPhone} = req.body;
        const user:TokenInterface = jwtDecode(req.body.token);

        const contact : Awaited<UserInterface|null> = await userFindOne({phoneNumber:contactPhone});
        if(!contact){
            res.status(404).json({data:"user not exist!"});
            return;
        }

        const contactID = contact._id;

        const contactIsExist : Awaited<ContactInterface|null> = await contactFindOne({
            contactPhone,
            user_id:user.user_id
        });
        
        if(contactIsExist){
            res.status(403).json({data:"you have this contact!"});
            return;
        }
        
        await contactCreateOne({
            ...req.body,
            username:user.username,
            contactID
        });

        res.status(202).json({data:"contact is created!"});
    }catch(err){
        res.status(500).json({data:err});
    }
}

export const queryContact = async (req:Request,res:Response)
    : Promise<void> => {
    try{
        const contactsArray : ContactInterface[] | null = [];

        const user:TokenInterface = jwtDecode(req.body.token);
        const contacts : Awaited<ContactInterface[]|null> = await contactFindMany({username:user.username});

        contacts?.forEach(item => {
            const {_id,contactName,contactPhone,username,contactID,notification} = item;
            contactsArray.push({_id,contactName,contactPhone,username,contactID,notification});
        });

        res.status(200).json({data:contactsArray});
    }catch(err){
        res.status(500).json({data:err});
    }
}

export const updateNotification = async (req:Request,res:Response)
    :Promise<void> => {
    try{
        const {token,notification,contactID} = req.body;
        var user:TokenInterface = jwtDecode(req.body.token);

        await contactFindOneAndUpdate(
            {
                $and:[
                    {contactID:contactID},
                    {username:user.username}
                ]
            },
            {notification:(notification === "on")?true:false}
        );

        res.status(200).json({data:{
            notification:(notification=="on")?true:false
        }})
    }catch(err){
        res.status(500).json({data:err});
    }
}

export const queryCurrentContact = async (req:Request,res:Response) 
    : Promise<void> => {
   try{
        const { username,contactID } = req.body;

        const currentContact:ContactInterface|null = await contactFindOneSelectColumn(
            {
                $and:[
                    { contactID },
                    { username }
                ]
            },
            [
                "notification","-_id"
            ],
            [
                "-_id","-email","-username","-password","-profilePicture","-phoneNumber","-createdAt","-updatedAt","-__v"
            ]
        );
        res.status(200).json(currentContact);
   }catch(err:unknown){
        res.status(500).json({data:err})
   }
}