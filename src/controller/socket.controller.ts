import { jwtDecode } from "jwt-decode";

import { userFindOne,userFindOneAndUpdate } from "../service/user.service";
import { messageCreateOne } from "../service/message.service";
import { UserInterface } from "../interface/user.interface";

interface MessageData {
    message:string,
    contactID:string,
    contactName:string,
    token:string
}

interface FileInterface {
    fileName:string,
    sender:string,
    contactName:string,
    contactID:string,
    messageType:string,
}

interface TokenInterface {
  username:string,
  user_id:string,
  exp:number,
  token:string
}

const online : Record<string,string> = {};
const ronline: Record<string,string> = {};

export const sendMessage = async (io:any,socket:any,data:any)
    :Promise<void> => {
    try{
        const {user_id,exp,username} = jwtDecode(data.token) as TokenInterface;
        const userIdExist : Awaited<UserInterface|null> = await userFindOne({_id:user_id});

        if(exp<(Date.now()/1000) || !userIdExist){
            io.to(socket.id).emit("fail",{});
            return;
        }

        const {message,contactID,contactName} = data;
        const messageIndex = {message,contactID,receiver:contactName,sender:username};

        online[contactName] && io.to(online[contactName]).emit(messageIndex);
        await messageCreateOne(messageIndex);
    }catch(err:unknown){
        console.log(err);
        io.to(socket.id).emit("catch-error",{error:err});
    }
}

export const onlineController = async (io:any,socket:any,data:MessageData)
    :Promise<void> => {
    try{
        const user:TokenInterface = jwtDecode(data.token) as TokenInterface;
        online[user.username] = socket.id;
        ronline[socket.id] = user.username;
    }catch(err:unknown){
        io.to(socket.id).emit('catch-error',{error:err});
    }
}

export const sendFile = async (io:any,socket:any,data:FileInterface)
    :Promise<void> => {
    try{
        const { contactID,fileName,messageType,contactName,sender } = data;
        const messageIndex = {message:fileName,contactID,receiver:contactName,sender,messageType,};

        online[contactName] && io.to(online[contactName]).emit(messageIndex);
    }catch(err:unknown){
        io.to(socket.id).emit('catch-error',{error:err});
    }
}

export const disconnect = async (io:any,socket:any)
    :Promise<void> => {
    try{
        await userFindOneAndUpdate(
            {
                username:ronline[socket.id]
            },
            {
                lastSeen:Date.now()
            }
        );
        const username:string = ronline[socket.id];
        online[username] && delete online[username];
        ronline[socket.id] && delete ronline[socket.id];
    }catch(err:unknown){
        io.to(socket.id).emit('catch-error',{error:err});
    }
}