import { join } from 'path';

import { Request,Response } from 'express';
import { generate } from 'shortid';

import { messageCreateOne,messageFindMany } from '../service/message.service';
import { userFindById, userFindOne } from '../service/user.service';
import { IMessage } from '../database/index';
import { UserInterface } from '../interface/user.interface';
import { MessageInterface } from '../interface/message.interface';
import { contactFindOne } from '../service/contact.service';
import { ContactInterface } from '../interface/contact.interface';
import { jwtDecode } from 'jwt-decode';
import { TokenInterface } from '../interface/token.interface';
import { isImage,isVideo,isVoice } from '../helper/fileTypes';
import { UploadedFile } from 'express-fileupload';

export const saveMessage = async (req:Request,res:Response) => {
    try{
        const {message,username,contactID,expireTime} = req.body;

        const userIsExist : UserInterface|null = await userFindById(contactID);
        
        if(Number(expireTime)<Date.now()/1000 || !userIsExist){
            res.status(406).json({
                data:"data isn't acceptable!"
            });
            return;
        }

        const messageIndex:IMessage = {
            message: message,
            sender: username,
            receiver: username,
            isSeened: true,
            contactID: contactID,
            isSaveMessage:true,
            messageType:"text"
        };

        const result : MessageInterface|null = await messageCreateOne(messageIndex);
        res.status(202).json({data:result});
    }catch(err){
        res.status(500).json({data:err});
    }
}

export const queryMessages = async (req:Request,res:Response) => {
    try{
        const { username,contactID,isSaveMessage } = req.body;

        if(isSaveMessage === "yes"){
            var messages : MessageInterface[]|null = await messageFindMany({
                $and:[
                    {isSaveMessage:true},
                    {sender:username},
                    {contactID:contactID}
                ]
            });
    
            res.status(202).json(messages);
        }else{
            const contactInfo : ContactInterface|null = await contactFindOne({username,contactID});

            var messages : MessageInterface[]|null = await messageFindMany({
                $or: [
                    { 
                        $and: [
                            {isSaveMessage:false},
                            {sender:username},
                            {receiver:contactInfo?.contactName}
                        ]
                    },
                    { 
                        $and: [
                            {isSaveMessage:false},
                            {sender:contactInfo?.contactName},
                            {receiver:username}
                        ]
                    }
                ]
            });
    
            res.status(202).json(messages);
        }
    }catch(err){
        res.status(500).json({data:err});
    }
}

export const sendFile = async (req:Request,res:Response) => {
    try{
        const file = req.files!.file as UploadedFile;
        const { token,contactID,contactName } = req.body;
        const decodedToken:TokenInterface = jwtDecode(token);

        const userIsExist:UserInterface|null = await userFindById(decodedToken.user_id);

        if((decodedToken.exp! < Date.now()/1000)||!userIsExist){
            res.status(451).json({data:"illegal request!"});
            return;
        }

        const fileTemp = file.name.split('.')[file.name.split('.').length - 1];
        
        if(
            isVoice(fileTemp.toLowerCase()) || 
            isImage(fileTemp.toLowerCase()) || 
            isVideo(fileTemp.toLowerCase())
        ){

            const fileName = `${generate()}.${fileTemp}`;

            file.mv(join(__dirname,'..','statics','uploads',fileName), 
                async(err:any):Promise<void> => {

                if(err){
                    res.status(422).json({data:err});
                };

                const contactInformation:Awaited<UserInterface|null> = await userFindById(contactID);

                const message:IMessage = {
                    message:fileName,
                    sender:decodedToken.username,
                    receiver:contactName,
                    isSeened:true,
                    contactID:contactID,
                    isSaveMessage:(decodedToken.username === contactInformation?.username)?true:false,
                    messageType: isImage(fileTemp.toLowerCase())?"image":(
                        isVideo(fileTemp.toLowerCase())?"video":(
                            isVoice(fileTemp.toLowerCase())?"voice":"text"
                        )
                    )
                }

                var result = await messageCreateOne(message);
                res.status(200).json(result);
            })

        }else{
            res.status(406).json({data:"File Not Acceptable!"});
        }
    }catch(err:unknown){
        res.status(500).json({data:err});
    }
}