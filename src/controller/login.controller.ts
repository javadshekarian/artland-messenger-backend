import { Request,Response } from "express";
import Joi from 'joi';
import bcrypt from 'bcryptjs';
import jwt from 'jsonwebtoken';
import { jwtDecode } from "jwt-decode";
import type {ObjectSchema} from 'joi';

import {contactCreateOne} from '../service/contact.service';
import { userCreateOne,userFindById,userFindByIdAndDelete,userFindByIdAndSelectColumn,userFindOne } from "../service/user.service";
import type { UserInterface } from "../interface/user.interface";
import {JWT_SECRET} from '../config/config';
import { TokenInterface } from "../interface/token.interface";

export const register = async (req:Request,res:Response) 
    : Promise<void> => {
    try{
        const {email,password} = req.body;
        const result : Awaited<UserInterface|null> = await userFindOne({email});
        if(!result){

            const registerValidation : ObjectSchema<any> = Joi.object().keys({
                username:Joi.string().trim().min(5),
                password:Joi.string().trim().min(5),
                email:Joi.string().email(),
                phoneNumber:Joi.string().min(10).max(15)
            });
            const {error} = registerValidation.validate(req.body);

            if(error){
                res.status(400).json({
                    data:error.details[0].message
                });
                return;
            }

            const hash : Awaited<string> = await bcrypt.hash(password,10);
            const result : Awaited<UserInterface|null> = await userCreateOne({
                ...req.body,
                password:hash
            });

            const token:string = jwt.sign(
                { 
                    user_id: result?._id,
                    email:result?.email,
                    username:result?.username,
                    phoneNumber:result?.phoneNumber
                },
                JWT_SECRET!,
                {
                    expiresIn:'24h'
                }
            );

            await contactCreateOne({
                contactName:"save message",
                contactPhone:result?.phoneNumber,
                username:result?.username,
                contactID:result?._id,
                notification:false,
            });

            res.status(202).json({
                token
            })
        }else{
            res.status(409).json({
                data:"user is already exist!"
            })
        }
    }catch(err){
        res.status(500).json({
            data:err
        })
    }
}

export const login = async (req:Request,res:Response)
    : Promise<void> => {
    try{
        const {username,password} = req.body;
        const result : Awaited<UserInterface|null> = await userFindOne({username});
        
        if(!result){
            res.status(404).json({
                data:"user not exist!"
            })
        }else{
            const checkPassword:Awaited<boolean> = await bcrypt.compare(password,result.password);
            
            if (checkPassword) {
                const token:string = jwt.sign(
                    { 
                        user_id: result._id, 
                        email:result.email,
                        username:result.username
                    },
                    JWT_SECRET!,
                    {
                        expiresIn:'24h'
                    }
                )
                res.status(200).json({token});
            } else {
                res.status(203).json({ data: "username or password is incorrect!" })
            }

        }
    }catch(err:unknown){
        res.status(500).json({
            data:err
        })
    }
}

export const checkToken = async (req:Request,res:Response)
    :Promise<void> => {
    try{
        const {token} = req.body;
        const decodedToken:any = jwtDecode(token);
        const user : Awaited<UserInterface|null> = await userFindOne({email:decodedToken.email});

        if(((Date.now()/1000)>decodedToken.exp)||!user){
            res.status(400).json({
                data:"invalid token"
            })
        }else{
            res.status(200).json({
                data:"valid token"
            })
        }
    }catch(err){
        res.status(500).json({
            data:err
        })
    }
}

export const queryAccountDataWithToken = async (req:Request,res:Response) 
    : Promise<void> => {
    try{
        const decodedToken:TokenInterface = jwtDecode(req.body.token);
        const user_id = decodedToken.user_id;

        const user : UserInterface|null = await userFindById(user_id);
        res.status(200).json(user);
    }catch(err:unknown){
        res.status(500).json({data:err});
    }
}

export const queryAccountDataWithUserID = async (req:Request,res:Response)
    : Promise<void> => {
    try{
        const user_id = req.body.user_id;
        const userInformation = await userFindByIdAndSelectColumn(
            user_id,
            ["profilePicture","-_id"]
        );
        res.status(200).json(userInformation);
    }catch(err:unknown){
        res.status(500).json({data:err});
    }
}