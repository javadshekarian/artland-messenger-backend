import {join} from 'path';

import { userFindOneAndUpdate } from '../service/user.service';

import { Request,Response } from "express";
import {generate} from 'shortid';
import {jwtDecode}  from 'jwt-decode';
import { UploadedFile } from 'express-fileupload';

interface JwtPayload {
    username:string,
    user_id:string,
    exp:number
}

export const uploadProfilePicture = (req:Request,res:Response)
    :any => {
    try{
        const profilePicture : UploadedFile|null = req.files?.profilePicture as UploadedFile;
        const {token} = req.body;
        const user = jwtDecode(token) as JwtPayload;
        const fileName:string = `${generate()}.jpg`;

        profilePicture.mv(
            join(__dirname,'..','statics','uploads',fileName),
            async (err:any) : Promise<void> => {

            if(err){
                res.status(415).json({data:err});
                return;
            };

            await userFindOneAndUpdate({_id:user.user_id},{profilePicture:fileName});
            res.status(202).json({data:fileName});

        });
    }catch(err:unknown){
        res.status(500).json({data:err});
    }
}