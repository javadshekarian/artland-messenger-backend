import path from 'path';

import dotenv from 'dotenv';

dotenv.config({path:path.join(__dirname,'config.env')});

const {NODE_ENV,PORT,MONGODB_URL,DATABASE_NAME,JWT_SECRET} = process.env;

export {
    NODE_ENV,PORT,MONGODB_URL,DATABASE_NAME,JWT_SECRET
}