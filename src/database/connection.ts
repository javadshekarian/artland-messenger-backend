import mongoose from "mongoose";
import * as config from "../config/config";

export const connectDB = async ():Promise<void> => {
    try {
        console.info("Connecting to database..." + config.MONGODB_URL);
        await mongoose.connect(`${config.MONGODB_URL}/${config.DATABASE_NAME}`);
        console.info("Database Is Connected!");
    } catch (error:unknown) {
        console.error(`Mongodb Connection Error: ${error}`);
        process.exit(1);
    }
};