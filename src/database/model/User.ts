import { Schema,model } from "mongoose";
import validator from "validator";

export interface IUser {
    email:string,
    username:string,
    password:string,
    profilePicture:string,
    phoneNumber:string,
    lastSeen?:Date
}

const UserSchema = new Schema<IUser>({
    email:{
        type:String,
        required:[true,"Email Can't Be Empty!"],
        unique:true,
        validate:[validator.isEmail,"Please Enter Valid Email!"],
        trim:true
    },
    username:{
        type:String,
        require:[true,"Username Can't Be Empty!"],
        unique:true,
        minlength:5,
        trim:true
    },
    password:{
        type:String,
        required:[true,"Password Can't Be Empty!"]
    },
    profilePicture:{
        type:String,
        default:''
    },
    phoneNumber:{
        type:String,
        require:[true,"Phone Number Can't Be Empty!"],
        trim:true
    },
    lastSeen:{
        type:Date,
        required:false
    }
},{
    timestamps:true
});

const User = model('users',UserSchema);
export default User;