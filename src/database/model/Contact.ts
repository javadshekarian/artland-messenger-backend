import { Schema,model,Types,Document } from "mongoose";

export interface IContact extends Document {
    contactName:string,
    contactPhone:string,
    username:string,
    contactID:any,
    notification?:boolean
}

const ContactSchema:Schema = new Schema<IContact>({
    contactName:{
        type:String,
        required:[true,"Contact Name Can't Be Empty!"],
        trim:true
    },
    contactPhone:{
        type:String,
        required:[true,"Contact Phone Can't Be Empty!"],
        trim:true
    },
    username:{
        type:String,
        required:true,
        trim:true
    },
    contactID:{
        type:Types.ObjectId,
        ref:"users"
    },
    notification:{
        type:Boolean,
        default:true,
        required:false
    }
});

const Contact = model<IContact>('contacts',ContactSchema);
export default Contact;