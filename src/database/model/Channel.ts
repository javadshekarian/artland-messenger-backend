import { Schema,Document,model,Types } from 'mongoose';

export interface IChannel extends Document {
    name:string,
    title:string,
    description:string,
    profilePictures:string[],
    members:Types.ObjectId[],
    channelID:string
}

const ChannelSchema:Schema = new Schema<IChannel>({
    name:{
        type:String,
        required:[true,"channel name can't be empty"],
        trim:true
    },
    title:{
        type:String,
        required:[true,"channel title can't be empty"],
        trim:true 
    },
    description:{
        type:String,
        required:[true,"channel description can't be empty"],
        trim:true 
    },
    profilePictures:{
        type:[String],
        required:false
    },
    members:{
        type:[Types.ObjectId]
    },
    channelID:{
        type:String,
        required:[true,"channelID can't be empty!"]
    }
});

const Channel = model('channels',ChannelSchema);
export default Channel;