import { Schema,Document,model,Types } from 'mongoose';

export interface IGroup extends Document {
    name:string,
    title:string,
    description:string,
    profilePictures:string[],
    members:Types.ObjectId[],
    groupID:string
}

const GroupSchema:Schema = new Schema<IGroup>({
    name:{
        type:String,
        required:[true,"group name can't be empty"],
        trim:true
    },
    title:{
        type:String,
        required:[true,"group title can't be empty"],
        trim:true 
    },
    description:{
        type:String,
        required:[true,"group description can't be empty"],
        trim:true 
    },
    profilePictures:{
        type:[String],
        required:false
    },
    members:{
        type:[Types.ObjectId]
    },
    groupID:{
        type:String,
        required:[true,"groupID can't be empty!"]
    }
});

const Group = model('groups',GroupSchema);
export default Group;