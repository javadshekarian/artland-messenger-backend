import { Schema,model,Types } from "mongoose"

export interface IMessage {
    message:string,
    sender:string,
    receiver:string,
    isSeened?:boolean,
    contactID:any,
    isSaveMessage?:boolean,
    messageType?:string
}

const MessageSchema = new Schema<IMessage>({
    message:{
        type:String,
        require:[true,"Message Can't Be Empty!"],
        trim:true
    },
    sender:{
        type:String,
        required:[true,"Sender Can't Be Empty!"]
    },
    receiver:{
        type:String,
        required:[true,"Receiver Can't Be Empty!"]
    },
    isSeened:{
        type:Boolean,
        default:false
    },
    contactID:{
        type:Types.ObjectId,
        ref:"users"
    },
    isSaveMessage:{
        type:Boolean,
        default:false,
        required:false
    },
    messageType:{
        type:String,
        default:'text',
        required:false,
        enum:["text","video","voice","image"]
    }
},{
    timestamps:true
});

const Message = model("messages",MessageSchema);
export default Message;