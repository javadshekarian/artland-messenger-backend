import User, { IUser } from "./model/User";
import Message, { IMessage } from "./model/Message";
import Contact, { IContact } from "./model/Contact";
import Group, { IGroup } from "./model/Group";
import Channel, { IChannel } from "./model/Channel";
import { connectDB } from "./connection";

export { 
    User,
    IUser,
    Message,
    IMessage,
    Contact,
    IContact,
    Group,
    IGroup,
    Channel,
    IChannel,
    connectDB
};