import { Contact } from "../database/index";
import { ContactInterface } from "../interface/contact.interface";

export const contactCreateOne = async (data:object) 
    : Promise<ContactInterface | null> => {
    const result = new Contact(data);
    await result.save();
    return result;
}

export const contactCreateMany = async (data:object) 
    : Promise<ContactInterface[] | null> => {
    const result = await Contact.insertMany(data);
    return result;
}

export const contactFindById = async (id:any) 
    : Promise<ContactInterface | null> => {
    const result = await Contact.findById(
        id,{__v:false}
    );
    return result;
}

export const contactFindOne = async (filter:object) 
    : Promise<ContactInterface | null> => {
    const result = await Contact.findOne(
        filter,{__v:false}
    ).populate({path:"contactID"});
    return result;
}

export const contactFindOneSelectColumn = async (filter:object,select:string[],populateSelect:string[])
    : Promise<ContactInterface | null> => {
    const result = await Contact
        .findOne(filter)
        .select(select)
        .populate({path:"contactID",select:populateSelect});

    return result;
}

export const contactFindMany = async (filter:object) 
    : Promise<ContactInterface[] | null> => {
    const result = await Contact.find(
        filter,{__v:false}
    ).populate({path:"contactID"});
    return result;
}

export const contactFindByIdAndUpdate = async (id:any,update:object) 
    : Promise<void> => {
    await Contact.findByIdAndUpdate(id,update);
}

export const contactFindOneAndUpdate = async (filter:object,update:object) 
    : Promise<ContactInterface|null> => {
    const result = await Contact.findOneAndUpdate(filter,update);
    return result;
}

export const contactUpdateMany = async (filter:object,update:object) 
    : Promise<void> => {
    await Contact.updateMany(filter,update);
}

export const contactFindOneAndDelete = async (filter:object) 
    : Promise<void> => {
    await Contact.deleteOne(filter);
}

export const contactFindByIdAndDelete = async (id:any) 
    : Promise<void> => {
    await Contact.findByIdAndDelete(id);
}

export const contactDeleteMany = async (filter:object) 
    : Promise<void> => {
    await Contact.deleteMany(filter);
}