import { User } from "../database/index";
import { UserInterface } from "../interface/user.interface";

export const userCreateOne = async (data:object) 
    : Promise<UserInterface | null> => {
    const result = new User(data);
    await result.save();
    return result;
}

export const userCreateMany = async (data:object) 
    : Promise<UserInterface[] | null> => {
    const result : UserInterface[] | null = await User.insertMany(data);
    return result;
}

export const userFindById = async (id:any) 
    : Promise<UserInterface | null> => {
    const result : UserInterface|null = await User.findById(
        id,
        {
            __v:false,
            createdAt:false,
            updateAt:false,
            password:false
        }
    );
    return result;
}

export const userFindOne = async (filter:object) 
    : Promise<UserInterface | null> => {
    const result : UserInterface|null = await User.findOne(
        filter,
        {
            __v:false,
            createdAt:false,
            updateAt:false
        }
    );
    return result;
}

export const userFindByIdAndSelectColumn = async (id:any,select:string[])
    : Promise<UserInterface|null> => {
    const result = await User
        .findById(id)
        .select(select);

    return result;
}

export const userFindMany = async (filter:object) 
    : Promise<UserInterface[] | null> => {
    const result : UserInterface[]|null = await User.find(
        filter,
        {
            __v:false,
            createdAt:false,
            updateAt:false,
            password:false
        }
    );
    return result;
}

export const userFindByIdAndUpdate = async (id:any,update:object) 
    : Promise<void> => {
    await User.findByIdAndUpdate(id,update);
}

export const userFindOneAndUpdate = async (filter:object,update:object) 
    : Promise<void> => {
    await User.findOneAndUpdate(filter,update);
}

export const userUpdateMany = async (filter:object,update:object) 
    : Promise<void> => {
    await User.updateMany(filter,update);
}

export const userFindOneAndDelete = async (filter:object) 
    : Promise<void> => {
    await User.deleteOne(filter);
}

export const userFindByIdAndDelete = async (id:any) 
    : Promise<void> => {
    await User.findByIdAndDelete(id);
}

export const userDeleteMany = async (filter:object) 
    : Promise<void> => {
    await User.deleteMany(filter);
}