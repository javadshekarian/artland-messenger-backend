import { Message } from "../database/index";
import { MessageInterface } from "../interface/message.interface";

export const messageCreateOne = async (data:object) 
    : Promise<MessageInterface | null> => {
    const result = new Message(data);
    await result.save();
    return result;
}

export const messageCreateMany = async (data:object) 
    : Promise<MessageInterface[] | null> => {
    const result = await Message.insertMany(data);
    return result;
}

export const messageFindById = async (id:any) 
    : Promise<MessageInterface | null> => {
    const result = await Message.findById(id,{__v:false});
    return result;
}

export const messageFindOne = async (filter:object) 
    : Promise<MessageInterface | null> => {
    const result = await Message.findOne(filter,{__v:false}).populate({
        path:"contactID",
        select:'-__v -createdAt -updatedAt -password'
    });
    return result;
}

export const messageFindMany = async (filter:object) 
    : Promise<MessageInterface[] | null> => {
    const result = await Message.find(filter,{__v:false}).populate({
        path:"contactID",
        select:'-__v -createdAt -updatedAt -password'
    });
    return result;
}

export const messageFindByIdAndUpdate = async (id:any,update:object) 
    : Promise<void> => {
    await Message.findByIdAndUpdate(id,update);
}

export const messageFindOneAndUpdate = async (filter:object,update:object) 
    : Promise<void> => {
    await Message.findOneAndUpdate(filter,update);
}

export const messageUpdateMany = async (filter:object,update:object) 
    : Promise<void> => {
    await Message.updateMany(filter,update);
}

export const messageFindOneAndDelete = async (filter:object) 
    : Promise<void> => {
    await Message.deleteOne(filter);
}

export const messageFindByIdAndDelete = async (id:any) 
    : Promise<void> => {
    await Message.findByIdAndDelete(id);
}

export const messageDeleteMany = async (filter:object) 
    : Promise<void> => {
    await Message.deleteMany(filter);
}