const voiceTypes:Record<string,boolean> = {
    "cda":true,"8svx":true,"webm":true,"wv":true,"wma":true,"wav":true,"vox":true,
    "voc":true,"tta":true,"sln":true,"rf64":true,"raw":true,"ra":true,"rm":true,
    "opus":true,"ogg":true,"oga":true,"mogg":true,"nmf":true,"msv":true,"mpc":true,
    "mp3":true,"movpkg":true,"mmf":true,"m4p":true,"m4b":true,"m4a":true,"ivs":true,
    "iklax":true,"gsm":true,"flac":true,"dvf":true,"dss":true,"awb":true,"au":true,
    "ape":true,"amr":true,"alac":true,"aiff":true,"act":true,"aax":true,"aac":true,
    "aa":true,"3gp":true
};
const videoTypes:Record<string,boolean> = {
    "webm":true,"mkv":true,"flv":true,"vob":true,"ogg":true,"ogv":true,"drc":true,
    "gif":true,"mng":true,"avi":true,"mts":true,"m2ts":true,"ts":true,"mov":true,
    "qt":true,"wmv":true,"yuv":true,"rm":true,"rmvb":true,"viv":true,"asf":true,
    "amv":true,"mp4":true,"m4p":true,"m4v":true,"mpg":true,"mp2":true,"mpeg":true,
    "mpe":true,"mpv":true,"m2v":true,"svi":true,"3gp":true,"3g2":true,"mxf":true,
    "roq":true,"nsv":true,"f4v":true,"f4p":true,"f4a":true,"f4b":true,
};

const imageTypes:Record<string,boolean> = {
    "jpg":true,"png":true,"gif":true,"webp":true,"tiff":true,"psd":true,"jpm":true,
    "bmp":true,"heif":true,"jpeg":true,"ai":true,"eps":true,"svgz":true,"jpx":true,
    "pdf":true,"tif":true,"raw":true,"arw":true,"cr2":true,"svg":true,"mj2":true,
    "nrw":true,"k25":true,"dib":true,"heic":true,"ind":true,"jpf":true,"j2k":true,
    "indd":true,"indt":true,"jp2":true,
}

export const isVoice = (fileTemp:string):boolean|undefined => {
    return voiceTypes[fileTemp];
}

export const isVideo = (fileTemp:string):boolean|undefined => {
    return videoTypes[fileTemp];
}

export const isImage = (fileTemp:string):boolean|undefined => {
    return imageTypes[fileTemp];
}