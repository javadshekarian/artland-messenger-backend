import { UserInterface } from "./user.interface";
import { MongodbInterface } from "./mongodb.interface";

export interface ContactInterface extends MongodbInterface {
    contactName:string,
    contactPhone:string,
    username:string,
    contactID:UserInterface,
    notification?:boolean
}