import { MongodbInterface } from "./mongodb.interface";

export interface UserInterface extends MongodbInterface {
    email:string,
    username:string,
    password:string,
    profilePicture:string,
    phoneNumber:string,
    lastSeen?:Date
}