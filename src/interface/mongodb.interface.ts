import { Types } from "mongoose";

export interface MongodbInterface {
    _id:Types.ObjectId,
    createAt?:Date,
    updateAt?:Date,
    __v?:number
}