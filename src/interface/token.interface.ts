export interface TokenInterface {
    user_id: string,
    email: string,
    username: string,
    phoneNumber: string,
    exp?:number
}