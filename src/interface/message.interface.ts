import { UserInterface } from "./user.interface";
import { MongodbInterface } from "./mongodb.interface";

export interface MessageInterface extends MongodbInterface{ 
    message:string,
    sender:string,
    receiver:string,
    isSeened?:boolean,
    contactID:UserInterface,
    isSaveMessage?:boolean,
    messageType?:string
}